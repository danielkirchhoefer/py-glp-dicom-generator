# -*- coding: utf-8 -*-
"""
Created on Sun Jul  5 12:56:02 2020

@author: danie
"""

import numpy as np
import os
import glob
from PIL import Image

source_path = "F:/Dokumente/Promotion/GummiBear/"
source_type = "tif"

file_path = "F:/Dokumente/Promotion/DICOM/Result_Images/data.dat"

pattern = source_path + "*." + source_type
fileList = glob.glob(pattern)
fileList.sort

im = Image.open(fileList[0])
image = np.array(im);
shape = image.shape
newShape = (len(fileList), shape[0], shape[1])
print(shape)
print(newShape)

fp = np.memmap(file_path, dtype='uint16', mode='w+', shape=newShape)
fp[0, :, :] = image
fp.flush()
im.close()

for i in range(1, len(fileList)):
    im = Image.open(fileList[i])
    image = np.array(im);
    fp[i, :, :] = image
    fp.flush()
    im.close()

del fp