# -*- coding: utf-8 -*-
"""
The MIT License (MIT)
Copyright (c) 2020 PCO AG

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
"""

import numpy as np

H_OFFSET_FACTOR = 5

class compression:

    # Property defines
    # --------------------------------------------------------------
    @property
    def bit_resolution(self):
        return self.__bit_res
    @property
    def bit_res_compressed(self):
        return self.__dest_bit
    @property
    def offset(self):
        return self.__offset

    @property
    def gain(self):
        return self.__gain

    @property
    def temp_dark_noise(self):
        return self.__dark_noise_e

    @property
    def dsnu(self):
        return self.__dsnu

    @property
    def prnu(self):
        return self.__prnu
    
    @property
    def rms_light_noise(self):
        return self.__light_noise_rms_pct

    @property
    def lut(self):
        return self.__lut
    
    @property
    def inverse_lut(self):
        return self.__inv_lut
    # --------------------------------------------------------------

    # --------------------------------------------------------------
    def __init__(self,
                 bit_resolution,
                 offset,
                 gain,
                 temp_dark_noise,
                 dsnu,
                 prnu,
                 light_noise=0.0,
                 dest_bit=8):
        """
        Initialize class members and compute the LUT based on it,
        according to paper DOI: https://doi.org/10.1515/teme-2019-0022

        Parameters
        ----------
        bit_resolution : int
            Resolution of the camera in bits
        offset : int
            Dark offset [DN] of the camera.
        gain : float
            Gain [DN/e-] of the camera.
        temp_dark_noise : float
            Temporal dark noise [e-] of the camera.
        dsnu : float
            DSNU [e-] of the camera.
        prnu : float
            PRNU [%] of the camera
        light_noise : float
            RMS noise [%] of light source (only set if known)
            The default is 0.
        dest_bit : float
            Bitness the compressed image should have
            The default is 8.

        Returns
        -------
        None.

        """
        self.__bit_res = bit_resolution
        self.__offset_DN = offset
        self.__gain_DNe = gain
        self.__dark_noise_e = temp_dark_noise
        self.__dsnu_e = dsnu
        self.__prnu_pct = prnu
        self.__light_noise_rms_pct = light_noise
        self.__dest_bit = dest_bit

        # Now compute the LUT and the inv lut.
        if self.__dest_bit > 8:
            self.__lut = np.zeros(65536, dtype='uint16')
        else:
            self.__lut = np.zeros(65536, dtype='uint8')
        self.__compute_lut()

        self.__inv_lut = np.zeros(2**self.__dest_bit, dtype='uint16')
        self.__compute_inv_lut()


    # -------------------------------------------------------------------------
    def compress_image(self, image):
        """
        Apply the lut to the transferred image to compress it to dest_bit

        Parameters
        ----------
        image : np.ndarray
            Image to be compressed.

        Raises
        ------
        ValueError
            If image type is not uint16.

        Returns
        -------
        np.ndarray
            Compressed image with destination bitness.

        """
        if image.dtype != 'uint16':
            raise ValueError("Image type must be uint16")

        # This call will use the dtype of lut and the range fo image,
        # so it does everything automatically
        return self.__lut[image]
    
        # -------------------------------------------------------------------------
    def decompress_image(self, image):
        """
        Apply the inverse lut to the transferred image to decompress it to 16 bit

        Parameters
        ----------
        image : np.ndarray
            Image to be decompressed.

        Raises
        ------
        ValueError
            If image type is invalid.

        Returns
        -------
        np.ndarray
            Decompressed 16 bit image.

        """
        
        if self.__dest_bit > 8 and image.dtype != 'uint16':
            raise ValueError("Image type must be uint16")
        elif image.dtype != 'uint8':
            raise ValueError("Image type must be uint8")

        # This call will use the dtype of lut and the range fo image,
        # so it does everything automatically
        return self.__inv_lut[image]
    
    def __compute_lut(self):
        """
        Compute the lut according to paper DOI: https://doi.org/10.1515/teme-2019-0022.

        Returns
        -------
        None.

        """
        sig_q2 = 1.0 / 12.0

        s = np.sqrt(np.power((0.01 * self.__prnu_pct), 2) + 
                    np.power((0.01 * self.__light_noise_rms_pct), 2))

        sig_0 = np.sqrt(np.power((self.__gain_DNe * self.__dsnu_e), 2) + 
                        np.power((self.__gain_DNe * self.__dark_noise_e), 2) +
                        sig_q2)

        g_max = int(np.power(2, self.__bit_res) - 1)
        g_max_corr = float(g_max - self.__offset_DN)
        h_max = (2**self.__dest_bit) - 1

        sig_h = h_max / (H_OFFSET_FACTOR + ((1.0 / s) * (np.log(2.0 * s * (np.sqrt((s**2) * (g_max_corr**2) + 
                                                                                   g_max_corr * self.__gain_DNe + 
                                                                                   sig_0**2) + 
                                                                           s * g_max_corr) + 
                                                                self.__gain_DNe) - 
                                                        np.log(2.0 * s * sig_0 + self.__gain_DNe))))

        # round down to 2 decimal places
        sig_h = np.floor(sig_h * 100.0) / 100.0;
        
        #First until offset
        g_dbl = 0.0
        h = 0.0
        for g in range(self.__offset_DN):
            g_dbl = g - self.__offset_DN
            h = H_OFFSET_FACTOR * sig_h + (sig_h / sig_0) * g_dbl
            if h < 0:
                self.__lut[g] = 0
            else:
                self.__lut[g] = np.round(h)
                
        for g in range(self.__offset_DN, g_max + 1): # g_max is included
            g_dbl = g - self.__offset_DN
            h = H_OFFSET_FACTOR * sig_h + (sig_h / s) * (np.log(2.0 * s * (np.sqrt(g_dbl * ((s**2) * g_dbl + self.__gain_DNe) + sig_0**2) + s * g_dbl) + 
                                                                self.__gain_DNe) - 
                                                         np.log(2.0 * s * sig_0 + self.__gain_DNe))
            if h < 0:
                self.__lut[g] = 0
            elif h > h_max:
                self.__lut[g] = h_max
            else:
                self.__lut[g] = np.round(h)
                
        np.savetxt('lut.txt', self.__lut, delimiter=',', fmt='%d')
        
    def __compute_inv_lut(self):
        """
        Compute the inverse lut according to paper DOI: https://doi.org/10.1515/teme-2019-0022.

        Returns
        -------
        None.

        """
        sig_q2 = 1.0 / 12.0

        s = np.sqrt(np.power((0.01 * self.__prnu_pct), 2) + 
                    np.power((0.01 * self.__light_noise_rms_pct), 2))

        sig_0 = np.sqrt(np.power((self.__gain_DNe * self.__dsnu_e), 2) + 
                        np.power((self.__gain_DNe * self.__dark_noise_e), 2) +
                        sig_q2)

        g_max = int(np.power(2, self.__bit_res) - 1)
        g_max_corr = float(g_max - self.__offset_DN)
        h_max = (2**self.__dest_bit) - 1

        sig_h = h_max / (H_OFFSET_FACTOR + ((1.0 / s) * (np.log(2.0 * s * (np.sqrt((s**2) * (g_max_corr**2) + 
                                                                                   g_max_corr * self.__gain_DNe + 
                                                                                   sig_0**2) + 
                                                                           s * g_max_corr) + 
                                                                self.__gain_DNe) - 
                                                        np.log(2.0 * s * sig_0 + self.__gain_DNe))))

        # round down to 2 decimal places
        sig_h = np.floor(sig_h * 100.0) / 100.0;
        
        #First until offset
        h_offset = int(np.round(H_OFFSET_FACTOR * sig_h))
        h_dbl = 0.0
        h = 0.0
        for h in range(h_offset):
            h_dbl = h - h_offset
            g = self.__offset_DN + (sig_0 / sig_h) * h_dbl
            if g < 0:
                self.__inv_lut[h] = 0
            else:
                self.__inv_lut[h] = np.round(g)
                
        for h in range(h_offset, h_max + 1): # h_max is included
            h_dbl = h - h_offset
            g = self.__offset_DN + ((np.power((np.exp(h_dbl * s / sig_h) * (sig_0 + (self.__gain_DNe / (2 * s))) -
                                            (self.__gain_DNe / (2 * s))), 2) - 
                                     sig_0**2) / 
                                    (np.exp(h_dbl * s / sig_h) * (2 * s * sig_0 + self.__gain_DNe)))

            if g < 0:
                self.__inv_lut[h] = 0
            elif g > g_max:
                self.__inv_lut[h] = g_max
            else:
                self.__inv_lut[h] = np.round(g)
                
        np.savetxt('inv_lut.txt', self.__inv_lut, delimiter=',', fmt='%d')
