# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 10:38:48 2020

@author: danie
"""

import glob
import pydicom
import yaml
import numpy as np
from decimal import Decimal
from typing import Tuple
from laplace_pyramid import laplace_pyramid


def merge_tiles(image_tiles: np.ndarray,
                image_size: Tuple,
                tile_size: Tuple) -> np.ndarray:
    
    """
    Combine the splitted image tiles to a list of images

    Parameters
    ----------
    image_tiles : np.ndarray
        A tiled 2D image plane.
    image_size : Tuple, optional
        Size of the actual image planes (height, width).
        The default is (1024, 1024).
    tile_size : Tuple, optional
        Size of the tiles (height, width).
        The default is (512, 512).

    Raises
    ------
    ValueError
        When tile size does not fit into image size.

    Returns
    -------
    images : np.ndarray
        A 2D image.

    """

    if (image_size[0] % tile_size[0]) != 0 or (image_size[1] % tile_size[1]) != 0:
        print("Invalid sizes, tile size must fit into image size")
        raise ValueError

    tiles_vertical = int(image_size[0] / tile_size[0])
    tiles_horizontal = int(image_size[1] / tile_size[1])

    cur_image = np.zeros(image_size, dtype=image_tiles[0].dtype)

    for h in range(tiles_vertical):
        for w in range(tiles_horizontal):
            cur_image[h*tile_size[0]:(h+1)*tile_size[0],
                      w*tile_size[1]:(w+1)*tile_size[1]] = image_tiles[h*tiles_horizontal + w]

    return cur_image


def regenerate_stack(file_dir: str) -> np.ndarray:
    """
    Load all planes from the dicom files and recreate a memmapped image stack

    Parameters
    ----------
    file_dir : str
        Path of the current level.

    Returns
    -------
     np.ndarray
            The recreated image.

    """

    pattern = file_dir + "*.dcm"
    dicom_files = glob.glob(pattern)
    dicom_files.sort

    dataset = pydicom.dcmread(dicom_files[0])
    tile_size = (dataset.Rows, dataset.Columns)
    cur_height = dataset.TotalPixelMatrixRows
    cur_width = dataset.TotalPixelMatrixColumns

    img_shape = (len(dicom_files), cur_height, cur_width)
    stack = np.memmap(file_dir + "stack.dat",
                      dtype=dataset.pixel_array.dtype,
                      mode='w+',
                      shape=img_shape)

    stack[0] = merge_tiles(dataset.pixel_array,
                           (cur_height, cur_width),
                           tile_size)

    for i in range(1, len(dicom_files)):
        dataset = pydicom.dcmread(dicom_files[i])
        stack[i] = merge_tiles(dataset.pixel_array,
                               (cur_height, cur_width),
                               tile_size)
        stack.flush()

    return stack


def main():
    with open("reconstruct.yml") as f:
        configuration = yaml.load(f, Loader=yaml.FullLoader)
        folder_path = configuration["pyramid_folder"]
        base_name = configuration["pyramid_name"]
        
        file_list = glob.glob(folder_path + base_name + "_peak_[0-99].dcm")

        if len(file_list) != 1:
            raise ValueError

        dataset = pydicom.dcmread(file_list[0])

        # Get Kernel
        kernel_list_dec = []
        for element in dataset.ConvolutionKernel._list:
            kernel_list_dec.append(Decimal(element))
        filter_kernel = np.array(kernel_list_dec)

        # Get Peak Level
        instance_nbr = int(dataset.InstanceNumber)
        peak_level = int(instance_nbr / 10000) - 1
        levels = dataset[0x00491013].value  # This is the levels tag
        isotropy_level = dataset[0x00491012].value  # This is the isotropy level

        image_list = []
        image_list.append(dataset.pixel_array)

        cur_level = peak_level - 1
        while cur_level >= 0:
            current_folder = folder_path + base_name + '{:02d}/'.format(cur_level)
            image = regenerate_stack(current_folder)
            image_list.append(image)
            cur_level -= 1

        pyramid = laplace_pyramid(filter_kernel)
        pyramid.info_from_peak(file_list[0])
        original_image_file = pyramid.reconstruct_image(image_list, folder_path, 0)
        
        print(original_image_file)


if __name__ == "__main__":
    main()