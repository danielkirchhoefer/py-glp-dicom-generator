The following example datasets are included

# Abdomen
An image stack from a CT Scan of a human abdomen.  
This stack is freely available under the following link:  
https://www.dicomlibrary.com/?manage=1b9baeb16d2aeba13bed71045df1bc65
