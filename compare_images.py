# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 18:33:41 2020

@author: danie
"""
import glob
import matplotlib.pyplot as plt
import numpy as np
import pydicom
import tifffile

folder_path = "F:/Dokumente/Promotion/DICOM/Result_Images/8Bit/"

#Load images
original = np.memmap(folder_path + "raw_data.dat", 
                     dtype='uint8',
                     mode='r',
                     shape=(272, 5120, 5120))
# restored = np.memmap(folder_path + "reconstruct.dat",
#                      dtype='uint8',
#                      mode='r',
#                      shape=(272, 5120, 5120))
itkCreated = tifffile.imread(folder_path + 'upscaleTest.tif')

min_diff = []
max_diff = []
diff_count = 0
for i in range(itkCreated.shape[0]):
    diff = original[i].astype('int') - itkCreated[i].astype('int')

    min_diff.append(np.min(diff))
    max_diff.append(np.max(diff))
    diff_count += np.count_nonzero(diff)

print("Max difference {}".format(np.max(max_diff)))
print("Min difference {}".format(np.min(min_diff)))
print("Diff count {}".format(diff_count))

plt.figure(0)
plt.imshow(original[0], cmap='gray')
plt.show()

plt.figure(1)
plt.imshow(itkCreated[0], cmap='gray')
plt.show()

