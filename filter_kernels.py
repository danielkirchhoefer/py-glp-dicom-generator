# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 18:19:34 2020

@author: danie
"""
import numpy as np
from decimal import Decimal


# -------------------------------------------------------------------------
def get_binomial_filter(size: int = 5):
    """
    Get the binomial 1D filter kernel according to the transferred size

    :param size: Size of the filter kernel (must be [5, 13])
    :type size: int

    :return:
    * filter_kernel: The required filter kernel
    """

    if size == 5:
        return np.array([Decimal(1) / Decimal(16),
                         Decimal(4) / Decimal(16),
                         Decimal(6) / Decimal(16),
                         Decimal(4) / Decimal(16),
                         Decimal(1) / Decimal(16)])
    elif size == 7:
        return np.array([Decimal(1) / Decimal(64),
                         Decimal(6) / Decimal(64),
                         Decimal(15) / Decimal(64),
                         Decimal(20) / Decimal(64),
                         Decimal(15) / Decimal(64),
                         Decimal(6) / Decimal(64),
                         Decimal(1) / Decimal(64)])
    elif size == 9:
        return np.array([Decimal(1) / Decimal(256),
                         Decimal(8) / Decimal(256),
                         Decimal(28) / Decimal(256),
                         Decimal(56) / Decimal(256),
                         Decimal(70) / Decimal(256),
                         Decimal(56) / Decimal(256),
                         Decimal(28) / Decimal(256),
                         Decimal(8) / Decimal(256),
                         Decimal(1) / Decimal(256)])
    elif size == 11:
        return np.array([Decimal(1) / Decimal(1024),
                         Decimal(10) / Decimal(1024),
                         Decimal(45) / Decimal(1024),
                         Decimal(120) / Decimal(1024),
                         Decimal(210) / Decimal(1024),
                         Decimal(252) / Decimal(1024),
                         Decimal(210) / Decimal(1024),
                         Decimal(120) / Decimal(1024),
                         Decimal(45) / Decimal(1024),
                         Decimal(10) / Decimal(1024),
                         Decimal(1) / Decimal(1024)])
    elif size == 13:
        return np.array([Decimal(1) / Decimal(4096),
                         Decimal(12) / Decimal(4096),
                         Decimal(66) / Decimal(4096),
                         Decimal(220) / Decimal(4096),
                         Decimal(495) / Decimal(4096),
                         Decimal(792) / Decimal(4096),
                         Decimal(924) / Decimal(4096),
                         Decimal(792) / Decimal(4096),
                         Decimal(495) / Decimal(4096),
                         Decimal(220) / Decimal(4096),
                         Decimal(66) / Decimal(4096),
                         Decimal(12) / Decimal(4096),
                         Decimal(1) / Decimal(4096)])
    else:
        raise ValueError("Filter size must be odd and in range [5, 13]")


# -------------------------------------------------------------------------
def get_optimized_filter(size: int = 5):
    """
    Get the optimized 1D filter kernel according to the transferred size

    :param size: Size of the filter kernel (must be [5, 13])
    :type size: int

    :return:
    * filter_kernel: The required filter kernel
    """

    if size == 5:
        return np.array([Decimal('0.13879'),
                         Decimal('0.22742'),
                         Decimal('0.26758'),
                         Decimal('0.22742'),
                         Decimal('0.13879')])
    elif size == 7:
        return np.array([Decimal('0.05333'),
                         Decimal('0.13142'),
                         Decimal('0.19888'),
                         Decimal('0.23274'),
                         Decimal('0.19888'),
                         Decimal('0.13142'),
                         Decimal('0.05333')])
    elif size == 9:
        return np.array([Decimal('0.02062'),
                         Decimal('0.06615'),
                         Decimal('0.12818'),
                         Decimal('0.18198'),
                         Decimal('0.20614'),
                         Decimal('0.18198'),
                         Decimal('0.12818'),
                         Decimal('0.06615'),
                         Decimal('0.02062')])
    elif size == 11:
        return np.array([Decimal('0.00806'),
                         Decimal('0.03160'),
                         Decimal('0.07345'),
                         Decimal('0.12514'),
                         Decimal('0.16853'),
                         Decimal('0.18644'),
                         Decimal('0.16853'),
                         Decimal('0.12514'),
                         Decimal('0.07345'),
                         Decimal('0.03160'),
                         Decimal('0.00806')])
    elif size == 13:
        return np.array([Decimal('0.00317'),
                         Decimal('0.01464'),
                         Decimal('0.03947'),
                         Decimal('0.07780'),
                         Decimal('0.12179'),
                         Decimal('0.15741'),
                         Decimal('0.17144'),
                         Decimal('0.15741'),
                         Decimal('0.12179'),
                         Decimal('0.07780'),
                         Decimal('0.03947'),
                         Decimal('0.01464'),
                         Decimal('0.00317')])
    else:
        raise ValueError("Filter size must be odd and in range [5, 13]")
