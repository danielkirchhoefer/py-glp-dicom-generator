# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 15:59:49 2020

@author: danie
"""

import pco
import numpy as np
import glob
import time
import yaml
import pydicom
from PIL import Image
from typing import Dict, Tuple
from filter_kernels import get_optimized_filter
from filter_kernels import get_binomial_filter
from laplace_pyramid import laplace_pyramid
from dicom_wsi_save import dicom_wsi_pyramid
from compression import compression


def prepare_shape(d: int, h: int, w: int) -> Tuple:
    """
    Extend shape of the image if necessary.

    Make width and height to be at least dividable by 32
    Make Depth to be at least dividable by 8
    Will leave the sizes unchanged if they already fulfill this

    Parameters
    ----------
    d : int
        Real depth.
    h : int
        Real height.
    w : int
        Real width.

    Returns
    -------
    Tuple
        Adapted (depth, height, width).

    """
    # For z resoulution we want images that can be divided at least by 8
    delta_d = 0
    if (d % 8) != 0:
        delta_d = 8 - (d % 8)
    # For x y resoulutio we want images that can be divided at least by 32
    delta_h = 0
    if (h % 32) != 0:
        delta_h = 32 - (h % 32)
    delta_w = 0
    if (w % 32) != 0:
        delta_w = 32 - (w % 32)

    return (d + delta_d, h + delta_h, w + delta_w)


def prepare_image(image: np.array) -> np.array:
    """
    Extend the image (2D) with the values of last row/col if necessary.

    Make width and height to be at least dividable by 32
    Will leave the image unchanged if it already fulfills this

    Parameters
    ----------
    image : np.array
        Image that might be extended.

    Returns
    -------
    image : np.array
        Adapted image
        (identical to original if the sizes are already dividable by 32).

    """
    h, w = image.shape
    # For x y resoulutio we want images that can be divided at least by 32
    rh = h % 32
    rw = w % 32
    if rh > 0:
        lines_to_add = 32 - rh
        for i in range(lines_to_add):
            image = np.append(image, image[-1:, :], axis=0)  # Replicate last line
    if rw > 0:
        cols_to_add = 32 - rw
        for i in range(cols_to_add):
            image = np.append(image, image[:, -1:], axis=1)  # Replicate last col

    return image


def generate_stack_files_tif(file_path: str,
                             file_ext: str,
                             save_path: str,
                             compress_params: Dict = None) -> [str, Tuple]:
    """
    Read in a list of images from a defined location and create a memmapped numpy array.

    Delete of memmapped array causes final flush to disc
    In this way also stacks exceeding the RAM can be created

    Depending on compress_params = None or not,
    loaded images are compressed before adding them to the stack
    
    This is the version for tif files

    Parameters
    ----------
    file_path : string
        Path to the folder where the source image files lie.
    file_ext : string
        File extension of the source files to search for.
    save_path : string
        Path where the recorded stack will be saved (name is raw_data.dat).
    compress_params : Dict, optional
        This object contains the compression parameters.
        If this is None, no compression will be used.
        The default is None.

    Returns
    -------
    [string, Tuple]
        The complete file path of the memmapped array file
        and a tuple containing the shape of the array are returned..

    """
    pattern = file_path + "*." + file_ext
    fileList = glob.glob(pattern)
    fileList.sort

    im = Image.open(fileList[0])
    image = np.array(im, dtype='uint16')
    shape = image.shape
    
    # Check if we have one multi or several single tiff
    isMutli = False
    img_shape = []
    if im.n_frames == 1 and len(fileList) > 1:
        isMutli = False
        img_shape = prepare_shape(len(fileList), shape[0], shape[1])
    elif im.n_frames > 1 and len(fileList) == 1:
        isMutli = True
        img_shape = prepare_shape(im.n_frames, shape[0], shape[1])
    else:
        raise ValueError("Either a list of single tiffs or one multi tiff is supported")

    compressor = None
    if compress_params is None:
        raw_stack = np.memmap(save_path + "raw_data.dat",
                              dtype='uint16',
                              mode='w+',
                              shape=img_shape)

        raw_stack[0, :, :] = prepare_image(image)
        for i in range(1, img_shape[0]):
            # Check if we have reached image count
            # if so, just append the last image file until we are done
            file_index = min(i, len(fileList)-1)
            if isMutli:
                im.seek(file_index)
            else:
                im = Image.open(fileList[file_index])

            image = np.array(im, dtype='uint16')
            image = prepare_image(image)  # Make it dividible by 32
            raw_stack[i, :, :] = image
            raw_stack.flush()
            im.close()
    else:
        raw_stack = np.memmap(save_path + "raw_data.dat",
                              dtype='uint8',
                              mode='w+',
                              shape=img_shape)

        compressor = compression(compress_params["bits"],
                                 compress_params["offset_DN"],
                                 compress_params["gain_DNe"],
                                 compress_params["temp_dark_noise_e"],
                                 compress_params["dsnu_e"],
                                 compress_params["prnu_pct"])

        raw_stack[0, :, :] = prepare_image(compressor.compress_image(image))
        for i in range(1, img_shape[0]):
            # Check if we have reached image count
            # if so, just append the last image file until we are done
            file_index = min(i, len(fileList)-1)
            if isMutli:
                im.seek(file_index)
            else:
                im = Image.open(fileList[file_index])

            image = compressor.compress_image(np.array(im, dtype='uint16'))
            image = prepare_image(image)  # Make it dividible by 32
            raw_stack[i, :, :] = image
            raw_stack.flush()
            im.close()

    del raw_stack

    return [(save_path + "raw_data.dat"), img_shape]


def generate_stack_files_dcm(file_path: str,
                             file_ext: str,
                             save_path: str,
                             compress_params: Dict = None) -> [str, Tuple]:
    """
    Read in a list of images from a defined location and create a memmapped numpy array.

    Delete of memmapped array causes final flush to disc
    In this way also stacks exceeding the RAM can be created

    Depending on compress_params = None or not,
    loaded images are compressed before adding them to the stack
    
    This is the version for dcm files

    Parameters
    ----------
    file_path : string
        Path to the folder where the source image files lie.
    file_ext : string
        File extension of the source files to search for.
    save_path : string
        Path where the recorded stack will be saved (name is raw_data.dat).
    compress_params : Dict, optional
        This object contains the compression parameters.
        If this is None, no compression will be used.
        The default is None.

    Returns
    -------
    [string, Tuple]
        The complete file path of the memmapped array file
        and a tuple containing the shape of the array are returned..

    """
    pattern = file_path + "*." + file_ext
    fileList = glob.glob(pattern)
    fileList.sort
    
    im = pydicom.dcmread(fileList[0])
    im_shape = im.pixel_array.shape
    
    isMutli = False
    img_shape = []
    if im.pixel_array.ndim == 2 and len(fileList) > 1:
        isMutli = False
        img_shape = prepare_shape(len(fileList), im_shape[0], im_shape[1])
        image = im.pixel_array
    elif im.pixel_array.ndim == 3 and len(fileList) == 1:
        isMutli = True
        img_shape = prepare_shape(im_shape[0], im_shape[1], im_shape[2])
        image = im.pixel_array[0]
    else:
        raise ValueError("Either a list of single tiffs or one multi tiff is supported")

    shape = image.shape
    
    # Check if we have one multi or several single tiff
    img_shape = prepare_shape(len(fileList), shape[0], shape[1])
    compressor = None
    if compress_params is None:
        raw_stack = np.memmap(save_path + "raw_data.dat",
                              dtype='uint16',
                              mode='w+',
                              shape=img_shape)

        raw_stack[0, :, :] = prepare_image(image)
        for i in range(1, img_shape[0]):
            # Check if we have reached image count
            # if so, just append the last image file until we are done
            file_index = min(i, len(fileList)-1)
            if isMutli:
                image = prepare_image(im.pixel_array[file_index])  # Make it dividible by 32
            else:
                im = pydicom.dcmread(fileList[file_index])
                image = prepare_image(im.pixel_array)  # Make it dividible by 32

            raw_stack[i, :, :] = image
            raw_stack.flush()
    else:
        raw_stack = np.memmap(save_path + "raw_data.dat",
                              dtype='uint8',
                              mode='w+',
                              shape=img_shape)

        compressor = compression(compress_params["bits"],
                                 compress_params["offset_DN"],
                                 compress_params["gain_DNe"],
                                 compress_params["temp_dark_noise_e"],
                                 compress_params["dsnu_e"],
                                 compress_params["prnu_pct"])

        raw_stack[0, :, :] = prepare_image(compressor.compress_image(image))
        for i in range(1, img_shape[0]):
            # Check if we have reached image count
            # if so, just append the last image file until we are done
            file_index = min(i, len(fileList)-1)
            
            if isMutli:
                image = compressor.compress_image(im.pixel_array[file_index])
                image = prepare_image(image)  # Make it dividible by 32
            else:
                im = pydicom.dcmread(fileList[file_index])
                image = compressor.compress_image(im.pixel_array)
                image = prepare_image(image)  # Make it dividible by 32

            raw_stack[i, :, :] = image
            raw_stack.flush()

    del raw_stack

    return [(save_path + "raw_data.dat"), img_shape]



def generate_stack_cam(image_count: int,
                       save_path: str,
                       compress_params: Dict = None) -> [str, Tuple]:
    """
    Record the images and store them in a memmapped numpy array.

    Delete of memmapped array causes final flush to disc
    In this way also stacks exceeding the RAM can be recorded

    Depending on compress_params = None or not, compession is used

    Parameters
    ----------
    image_count : int
        Number of images to record (i.e. depth of the stack).
    save_path : string
        Path where the recorded stack will be saved (name is raw_data.dat).
    compress_params : Dict, optional
        This object contains the compression parameters.
        If this is None, no compression will be used.
        The default is None.

    Returns
    -------
    [string, Tuple]
        The complete file path of the memmapped array file
        and a tuple containing the shape of the array are returned.

    """
    image = []
    img_shape = ()
    raw_stack = None
    with pco.Camera() as cam:
        cam.configuration = {'exposure time': 2e-3}  # 'timestamp': 'binary & ascii'
        settings = cam.sdk.get_sizes()
        img_shape = prepare_shape(image_count, settings["y"], settings["x"])

        if compress_params is None:
            raw_stack = np.memmap(save_path + "raw_data.dat",
                                  dtype='uint16',
                                  mode='w+',
                                  shape=img_shape)
        else:
            raw_stack = np.memmap(save_path + "raw_data.dat",
                                  dtype='uint8',
                                  mode='w+',
                                  shape=img_shape)

            cam.rec.set_compression_parameter(gain=compress_params["gain_DNe"],
                                              dark_noise=compress_params["temp_dark_noise_e"],
                                              dsnu=compress_params["dsnu_e"],
                                              prnu=compress_params["prnu_pct"])

        cam.record(image_count, 'sequence non blocking')
        for i in range(img_shape[0]):
            # Check if we have reached image count
            # if so, just append the last recorded image until we are done
            img_index = min(i, image_count-1)

            cam.rec.get_status()["dwProcImgCount"]
            while cam.rec.get_status()["dwProcImgCount"] <= img_index:
                time.sleep(10)

            if compress_params is None:
                [image, _] = cam.image(img_index)
                image = prepare_image(image)  # Make it dividible by 32
                raw_stack[i, :, :] = image
            else:
                [image, _] = cam.image_compressed(img_index)
                image = prepare_image(image)  # Make it dividible by 32
                raw_stack[i, :, :] = image

            raw_stack.flush()

    del raw_stack

    return [(save_path + "raw_data.dat"), img_shape]


def main():
    with open("config.yml") as f:
        configuration = yaml.load(f, Loader=yaml.FullLoader)
        
        bits = configuration["bits"]
        img_type = configuration["datatype"]
        xy_res = configuration["pixel_size_um"]
        z_res = configuration["pixel_depth_um"]
        
        base_name = configuration["pyramid_name"]
        file_path = configuration["dest_path"]
        file_compression = configuration["file_compression"]
        
        kernel = []
        if configuration["filter_type"] == "optimized":
            kernel = get_optimized_filter(configuration["kernel_size"])
        elif configuration["filter_type"] == "binomial":
            kernel = get_binomial_filter(configuration["kernel_size"])
        else:
            raise ValueError("Invalid filter kernel type")
            
        compress_on = configuration["use_compression"]
        compress_dict = configuration["compression_parameters"]
        
        from_files = configuration["from_files"]
        source_path = configuration["src_path"]
        source_type = configuration["file_type"]
        number_of_images = configuration["number_of_images"]
        
        if compress_on:
            bits = 8  # Since we are using compression
            img_type = 'uint8'
        else:
            compress_dict = None
        
        pyramid = laplace_pyramid(kernel)
        
        dicom_saver = dicom_wsi_pyramid(file_path, base_name, compression=file_compression)
        
        stack_path = ""
        stack_shape = ()
        if from_files:
                if source_type == "tif":
                    [stack_path, stack_shape] = generate_stack_files_tif(source_path,
                                                                         source_type,
                                                                         file_path,
                                                                         compress_dict)
                elif source_type == "dcm":
                    [stack_path, stack_shape] = generate_stack_files_dcm(source_path,
                                                                         source_type,
                                                                         file_path,
                                                                         compress_dict)
                else:
                    raise ValueError("Unsupported file extension")
        else:
            [stack_path, stack_shape] = generate_stack_cam(number_of_images, file_path)
        
        images = np.memmap(stack_path,
                           dtype=img_type,
                           mode='r',
                           shape=stack_shape)
        
        pyramid.from_image_stack(file_path,
                                 images,
                                 anisotropy=z_res/xy_res,
                                 bits_per_pixel=bits)
        
        h, w = images[0].shape
        peak_image = pyramid.get_pyramid_level(pyramid.peak_level)
        dicom_saver.set_image_info(image_size=(stack_shape[0],
                                               stack_shape[1],
                                               stack_shape[2]),
                                   pixel_size=(z_res, xy_res, xy_res),
                                   tile_size=peak_image[0].shape,
                                   levels=(pyramid.peak_level + 1),
                                   isotropy_level=pyramid.isotropy_level,
                                   bits_per_pixel=bits,
                                   filter_kernel=np.float32(pyramid.filter_kernel),
                                   meta=None)
        
        for lev in range(pyramid.peak_level + 1):
            cur_xy_res = xy_res * 2**lev
            if lev > pyramid.isotropy_level:
                cur_pix_size = (pyramid.anisotropy*cur_xy_res, cur_xy_res, cur_xy_res)
            else:
                cur_pix_size = (z_res, cur_xy_res, cur_xy_res)
        
            if lev == pyramid.peak_level:
                dicom_saver.save_peak_level(pyramid.get_pyramid_level(lev),
                                            lev,
                                            cur_pix_size)
            else:
                dicom_saver.save_pyramid_level(pyramid.get_pyramid_level(lev),
                                               lev,
                                               cur_pix_size)
        
        pyramid.remove_memmap_files()


if __name__ == "__main__":
    main()
