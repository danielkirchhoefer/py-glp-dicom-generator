# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 16:28:09 2020

@author: danie
"""

import numpy as np
from decimal import Decimal
from typing import Dict, Tuple, List
import pydicom
import cv2
import os
import gc

class laplace_pyramid:

    # Property defines
    # --------------------------------------------------------------
    @property
    def filter_radius(self):
        return self.__filter_radius

    @property
    def filter_kernel(self):
        return self.__filter_kernel

    @property
    def anisotropy(self):
        return self.__anisotropy

    @property
    def isotropy_level(self):
        return self.__isotropy_level

    @property
    def peak_level(self):
        return self.__peak_level

    @property
    def default_value(self):
        return self.__default_value

    @property
    def dtype(self):
        return self.__dtype

    # No Real Property
    def get_pyramid_level(self, level):
        return np.memmap(self.__pyramid[level],
                         dtype=self.__dtype,
                         mode='r',
                         shape=self.__pyramid_shape[level])
    # --------------------------------------------------------------

    # --------------------------------------------------------------
    def __init__(self, filter_kernel: np.ndarray):
        """
        Initialize class members

        Parameters
        ----------
        filter_kernel : np.ndarray
            Array of decimals representing the filter kernel to be used
            (must have an odd length)

        Raises
        ------
        ValueError
            When filter kernel has an even length.

        Returns
        -------
        None.

        """

        if (filter_kernel.size % 2) == 0:
            raise ValueError("Filter must have an odd length")

        self.__pyramid = []
        self.__pyramid_shape = []
        self.__pyramid_file_list = []
        self.__filter_kernel = filter_kernel
        self.__filter_radius = int((filter_kernel.size - Decimal(1)) / Decimal(2))
        self.__anisotropy = 10  # Just a default value
        self.__isotropy_level = 5
        self.__peak_level = 10
        self.__default_value = 128
        self.__dtype = 'uint8'

    # --------------------------------------------------------------
    def remove_memmap_files(self):
        # Make sure everything is deleted
        #for levels in self.__pyramid:
        #    del levels
        #gc.collect() # To really free the files
        print(self.__pyramid_file_list)
        #for file in self.__pyramid_file_list:
        #    os.remove(file)

    # --------------------------------------------------------------
    def from_image_stack(self,
                         source_path: str,
                         images: np.ndarray,
                         anisotropy: float = 10,
                         stop_level: int = -1,
                         bits_per_pixel: int = 8):
        """
        Create the laplacian pyramid out of the transferred image stack.
        Here the pyramid levels are automatically defined, as long as the size
        is larger than the tile size, a new pyramid level will be generated

        Parameters
        ----------
        source_path : str
            Path where the memmapped raw images should be saved
        images : np.ndarray
            Stack of images where the pyramid should be created for
            Image always has to be LSB aligned.
        anisotropy : float, optional
            The anisotropy between xy and z direction.
            The default is 10.
        stop_level : int, optional
            Level where the pyramid should be stopped (starts with level 0).
            If this is set to -1 pyramid will be created,
            as long as the resolution is a multiple of 2.
            The default is -1.
        bits_per_pixel : int, optional
            Number of bits per pixel for the image.
            The default is 8.

        Raises
        ------
        ValueError
            When the image type is neither uint8 nor uint16 or invalid bitness.

        Returns
        -------
        None.

        """

        self.__anisotropy = anisotropy
        self.__dtype = images.dtype

        if bits_per_pixel > 16:
            print("Invalid bitness, must be <= 16")
            raise ValueError

        if self.__dtype == 'uint16':
            self.__default_value = 32768
        elif self.__dtype == 'uint8':
            self.__default_value = 128
            if bits_per_pixel > 8:
                print("Invalid bitness, must be <= 8 for 8 Bit data")
                raise ValueError
        else:
            print("Invalid Image format, only uint8 and uint16 are supported")
            raise ValueError

        depth, height, width = images.shape

        if stop_level <= 0:
            stop_level = max(np.log2(depth), np.log2(height), np.log2(width))

        level = 0
        # For <=1.3 isotropy is better without further reduction
        d = depth
        h = height
        w = width
        # 4/3 divided by 2 = 2/3 and both have the same absolute anisotropy
        # -> alle values below 4/3 would worsen the isotropy if we go 2D
        # smaller than 50 x 50 makes no real sense
        while anisotropy > (4.0 / 3.0) and w > 50 and h > 50:
            if h % 2 != 0 or w % 2 != 0:
                break
            
            # For 8 Bit data we also make sure that we have rows with WORD padding
            # Therefore width hast to be a multiple of 2 and thus we break on w % 4 != 0
            if(self.__dtype == 'uint8') and (w % 4 != 0):
                break;
            
            baseImage = np.memmap(source_path + "base{:02d}.dat".format(level+1),
                                       dtype=self.__dtype,
                                       mode='w+',
                                       shape=(d, int(h/2), int(w/2)))

            laplace_image = np.memmap(source_path + "laplace{:02d}.dat".format(level),
                                      dtype=self.__dtype,
                                      mode='w+',
                                      shape=(d, h, w))

            self.__pyramid_file_list.append(baseImage.filename)
            self.__pyramid_file_list.append(laplace_image.filename)
            
            for td in range(d):
                baseImage[td], laplace_image[td] = self.do_laplace_2D(images[td])

            self.__pyramid.append(laplace_image.filename)
            self.__pyramid_shape.append(laplace_image.shape)
            d, h, w = baseImage.shape
            anisotropy /= 2.0
            
            # Flush to file
            del laplace_image
            del baseImage
            del images
            
            images = np.memmap(source_path + "base{:02d}.dat".format(level+1),
                                       dtype=self.__dtype,
                                       mode='r',
                                       shape=(d, h, w))
            level += 1

        self.__anisotropy = anisotropy  # Anisotropy that remains
        self.__isotropy_level = level

        #smaller than 64 x 64 makes no real sense
        while d % 2 == 0 and h % 2 == 0 and w % 2 == 0 and w > 64 and h > 64:
            # For 8 Bit data we also make sure that we have rows with WORD padding
            # Therefore width hast to be a multiple of 2 and thus we break on w % 4 != 0
            if(self.__dtype == 'uint8') and (w % 4 != 0):
                break;
                
            baseImage = np.memmap(source_path + "base{:02d}.dat".format(level+1),
                                       dtype=self.__dtype,
                                       mode='w+',
                                       shape=(int(d/2), int(h/2), int(w/2)))
            laplace_image = np.memmap(source_path + "laplace{:02d}.dat".format(level),
                                      dtype=self.__dtype,
                                      mode='w+',
                                      shape=(d, h, w))

            self.__pyramid_file_list.append(baseImage.filename)
            self.__pyramid_file_list.append(laplace_image.filename)
            
            baseImage[:], laplace_image[:] = self.do_laplace_3D(images,
                                                                source_path,
                                                                level)
            self.__pyramid.append(laplace_image.filename)
            self.__pyramid_shape.append(laplace_image.shape)
            d, h, w = baseImage.shape

            # Flush to file
            del laplace_image
            del baseImage
            del images
            
            images = np.memmap(source_path + "base{:02d}.dat".format(level+1),
                                       dtype=self.__dtype,
                                       mode='r',
                                       shape=(d, h, w))
            level += 1

        self.__pyramid.append(images.filename)  # Append peak image
        self.__pyramid_shape.append(images.shape)
        self.__peak_level = level
        
        del images

    # --------------------------------------------------------------
    def info_from_peak(self, file_path: str):
        """
        Fetch all the necessary information using the peak image dicom file

        Parameters
        ----------
        file_path : str
            Path to the file representing the peak of the laplacian pyramid.

        Raises
        ------
        ValueError
            When the image type is neither uint8 nor uint16.

        Returns
        -------
        None.

        """

        dataset = pydicom.dcmread(file_path)
        number_of_frames = int(dataset.NumberOfFrames)
        instance_nbr = int(dataset.InstanceNumber)
        self.__peak_level = int(instance_nbr / 10000) - 1

        cur_depth = dataset.TotalPixelMatrixFocalPlanes
        cur_height = dataset.TotalPixelMatrixRows
        cur_width = dataset.TotalPixelMatrixColumns

        tile_size = (dataset.Rows, dataset.Columns)
        self.__isotropy_level = dataset[0x00491012].value
         
        cur_pixel_size_um = [1, 1, 1]
        pixel_size_um = [1, 1, 1]
        raw_size_pix = [1, 1, 1]

        cur_pixel_size_um[0] = dataset[0x00491003].value
        cur_pixel_size_um[1] = dataset[0x00491002].value
        cur_pixel_size_um[2] = dataset[0x00491001].value
        self.__anisotropy = cur_pixel_size_um[0] / cur_pixel_size_um[2]

        pixel_size_um[0] = cur_pixel_size_um[0] / (2**self.__peak_level)
        pixel_size_um[1] = cur_pixel_size_um[1] / (2**self.__peak_level)
        pixel_size_um[2] = cur_pixel_size_um[2] / (2**self.__peak_level)

        raw_size_pix[0] = np.round(dataset.ImagedVolumeDepth * 1000 / cur_pixel_size_um[0]).astype(int)
        raw_size_pix[1] = np.round(dataset.ImagedVolumeHeight * 1000 / cur_pixel_size_um[1]).astype(int)
        raw_size_pix[2] = np.round(dataset.ImagedVolumeWidth * 1000 / cur_pixel_size_um[2]).astype(int)

        self.__dtype = dataset.pixel_array.dtype
        if self.__dtype == 'uint16':
            self.__default_value = 32768
        elif self.__dtype == 'uint8':
            self.__default_value = 128
        else:
            print("Invalid Image format, only uint8 and uint16 are supported")
            raise ValueError

    # --------------------------------------------------------------
    def reconstruct_image(self,
                          image_list: List,
                          source_path: str,
                          level: int = 0) -> str:
        """
        Recreate an image out of the pyramid that is currently set

        Parameters
        ----------
        image_list: List
            List of 3D numpy arrays containing the images for the pyramid
        source_path: List
            Path of the main folder
        level : int, optional
            The level down to witch the pyramid should be generated.
            The default is 0.

        Raises
        ------
        ValueError
            When level is invalid.

        Returns
        -------
        filename : str
            File path of the econstructed 3D image.

        """
        if level < 0 or level > self.__peak_level:
            print("Invalid level, pyramid has only {} levels".format(self.__peak_level))
            raise ValueError

        base_image = image_list.pop(0)

        cur_level = self.__peak_level - 1
        while cur_level >= level and cur_level >= self.__isotropy_level:
            d, h, w = base_image.shape
            laplace_image = image_list.pop(0)
            
            mmap_file_name = "reconstruct_{:02d}.dat".format(cur_level+1)
            if cur_level == level:
                mmap_file_name = "original_image.dat"

            image = np.memmap(source_path + mmap_file_name,
                              dtype=self.__dtype,
                              mode='w+',
                              shape=((d * 2), (h * 2), (w * 2)))
            image[:] = self.do_inverse_laplace_3D(base_image,
                                                  laplace_image)
            del image
            del base_image
            del laplace_image

            base_image = np.memmap(source_path + mmap_file_name,
                                   dtype=self.__dtype,
                                   mode='r',
                                   shape=((d * 2), (h * 2), (w * 2)))
            cur_level -= 1
            
        while cur_level >= level:
            d, h, w = base_image.shape
            laplace_image = image_list.pop(0)

            mmap_file_name = "reconstruct_{:02d}.dat".format(cur_level+1)
            if cur_level == level:
                mmap_file_name = "original_image.dat"

            image = np.memmap(source_path + mmap_file_name,
                              dtype=self.__dtype,
                              mode='w+',
                              shape=(d, (h * 2), (w * 2)))
            for td in range(d):
                image[td] = self.do_inverse_laplace_2D(base_image[td],
                                                       laplace_image[td])

            del image
            del base_image
            del laplace_image

            base_image = np.memmap(source_path + mmap_file_name,
                                   dtype=self.__dtype,
                                   mode='r',
                                   shape=(d, (h * 2), (w * 2)))
            cur_level -= 1
        
        filename = base_image.filename
        del base_image
        
        return filename

    # --------------------------------------------------------------
    def do_laplace_2D(self,
                      image: np.ndarray) -> [np.ndarray, np.ndarray]:
        """
        Create the laplace image as well as the reduced image from the
        transferred image

        Filtering is done using openCV filter2d with BORDER_REFLECT_101,
        which means gfedcb|abcdefgh|gfedcba

        Parameters
        ----------
        image : np.ndarray
            Image where the laplacew image should be created from
            Image always has to be LSB aligned.

        Returns
        -------
        reduced_image : np.ndarray
            The image after the REDUCE function.
        laplace_image : np.ndarray
            The image difference between original and image after.

        """

        kernel_2d = np.outer(self.__filter_kernel, self.__filter_kernel)

        # Do the reduce
        reduced_image = cv2.filter2D(src=image,
                                     ddepth=-1,
                                     kernel=np.float32(kernel_2d),
                                     borderType=cv2.BORDER_REFLECT_101)[::2, ::2]

        # Do the expand
        # In EXPAND a scaling is needed, because not all pixels are used
        # Either all even or all odd elemnts of the kernel are used
        # So there are 2 states and since we are in 2D, it results in 4 cases

        start_even = self.__filter_radius % 2
        start_odd = (self.__filter_radius + 1) % 2

        scaling = np.zeros((2, 2), dtype='float')
        scaling[0, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                      np.sum(self.__filter_kernel[start_even::2]))

        scaling[0, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                      np.sum(self.__filter_kernel[start_odd::2]))

        scaling[1, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                      np.sum(self.__filter_kernel[start_even::2]))

        scaling[1, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                      np.sum(self.__filter_kernel[start_odd::2]))

        expand_image = np.zeros(image.shape, dtype='float')
        expand_image[::2, ::2] = reduced_image
        expand_image = cv2.filter2D(src=expand_image,
                                    ddepth=-1,
                                    kernel=np.float32(kernel_2d),
                                    borderType=cv2.BORDER_REFLECT_101)

        # Adapt using the scaling
        expand_image[0::2, 0::2] *= scaling[0, 0]
        expand_image[0::2, 1::2] *= scaling[0, 1]
        expand_image[1::2, 0::2] *= scaling[1, 0]
        expand_image[1::2, 1::2] *= scaling[1, 1]
        expand_image = np.round(expand_image).astype(image.dtype)

        laplace_image = image - expand_image + self.__default_value

        return reduced_image, laplace_image

    # --------------------------------------------------------------
    def do_laplace_3D(self,
                      image: np.ndarray,
                      source_path: list,
                      level: int) -> [np.ndarray, np.ndarray]:
        """
        Create the laplace image as well as the reduced image from the
        transferred image

        Filtering is done using openCV filter2d with BORDER_REFLECT_101,
        which means gfedcb|abcdefgh|gfedcba

        Parameters
        ----------
        image : np.ndarray
            Image where the laplacew image should be created from
            Image always has to be LSB aligned..
        source_path: List
            Path of the main folder, used just for name of the memmap array
        level : int
            Current level of the pyramid, used just as index of the memmap array

        Returns
        -------
        reduced_image : np.ndarray
            The image after the REDUCE function.
        laplace_image : np.ndarray
            The image difference between original and image after.

        """

        kernel_2d = np.outer(self.__filter_kernel, self.__filter_kernel)
        d, h, w = image.shape
        R = self.__filter_radius
        # Do the reduce
        reduced_image = np.copy(image)

        for th in range(h):
            for tw in range(w):
                curLine = np.copy(reduced_image[:, th, tw])
                curLine = np.concatenate((curLine[R:0:-1],
                                         curLine,
                                         curLine[(d-2):(d-2-R):-1]))
                reduced_image[:, th, tw] = np.convolve(curLine,
                                                       np.float32(self.__filter_kernel),
                                                       'same')[R:(d+R)]
        for td in range(0, d, 2):  # The odd images do not matter
            reduced_image[td] = cv2.filter2D(src=reduced_image[td],
                                             ddepth=-1,
                                             kernel=np.float32(kernel_2d),
                                             borderType=cv2.BORDER_REFLECT_101)
        reduced_image = reduced_image[::2, ::2, ::2]

        # Do the expand
        # In EXPAND a scaling is needed, because not all pixels are used
        # Either all even or all odd elemnts of the kernel are used
        # So there are 2 states and since we are in 3D, it results in 8 cases

        start_even = self.__filter_radius % 2
        start_odd = (self.__filter_radius + 1) % 2

        scaling = np.zeros((2, 2, 2), dtype='float')
        scaling[0, 0, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[0, 0, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        scaling[0, 1, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[0, 1, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        scaling[1, 0, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[1, 0, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        scaling[1, 1, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[1, 1, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        expand_image = np.memmap(source_path + "expand_{:02d}.dat".format(level),
                                 dtype='float',
                                 mode='w+',
                                 shape=image.shape)

        expand_image[::2, ::2, ::2] = reduced_image

        for t_h in range(h):
            for t_w in range(w):
                curLine = np.copy(expand_image[:, t_h, t_w])
                curLine = np.concatenate((curLine[R:0:-1],
                                          curLine,
                                          curLine[(d-2):(d-2-R):-1]))
                expand_image[:, t_h, t_w] = np.convolve(curLine,
                                                        np.float32(self.__filter_kernel),
                                                        'same')[R:(d+R)]
        for t_d in range(d):
            expand_image[t_d] = cv2.filter2D(src=expand_image[t_d],
                                             ddepth=-1,
                                             kernel=np.float32(kernel_2d),
                                             borderType=cv2.BORDER_REFLECT_101)

        # Adapt using the scaling
        expand_image[0::2, 0::2, 0::2] *= scaling[0, 0, 0]
        expand_image[0::2, 0::2, 1::2] *= scaling[0, 0, 1]
        expand_image[0::2, 1::2, 0::2] *= scaling[0, 1, 0]
        expand_image[0::2, 1::2, 1::2] *= scaling[0, 1, 1]
        expand_image[1::2, 0::2, 0::2] *= scaling[1, 0, 0]
        expand_image[1::2, 0::2, 1::2] *= scaling[1, 0, 1]
        expand_image[1::2, 1::2, 0::2] *= scaling[1, 1, 0]
        expand_image[1::2, 1::2, 1::2] *= scaling[1, 1, 1]
        expand_image = np.round(expand_image).astype(image.dtype)

        laplace_image = image - expand_image + self.__default_value

        del expand_image
        return reduced_image, laplace_image

        # --------------------------------------------------------------
    def do_inverse_laplace_2D(self,
                              base_image: np.ndarray,
                              laplace_image: np.ndarray) -> np.ndarray:
        """
        Recreate an image out of the base (=reduced) and the laplace image

        Filtering is done using openCV filter2d with BORDER_REFLECT_101,
        which means gfedcb|abcdefgh|gfedcba

        Parameters
        ----------
        base_image : np.ndarray
            Base image.
        laplace_image : np.ndarray
            Laplace image.

        Returns
        -------
        np.ndarray
            The recreated image with the same size than the laplace image.

        """

        kernel_2d = np.outer(self.__filter_kernel, self.__filter_kernel)
        h, w = laplace_image.shape

        # Do the expand
        # In EXPAND a scaling is needed, because not all pixels are used
        # Either all even or all odd elemnts of the kernel are used
        # So there are 2 states and since we are in 2D, it results in 4 cases

        start_even = self.__filter_radius % 2
        start_odd = (self.__filter_radius + 1) % 2

        scaling = np.zeros((2, 2), dtype='float')
        scaling[0, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                      np.sum(self.__filter_kernel[start_even::2]))

        scaling[0, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                      np.sum(self.__filter_kernel[start_odd::2]))

        scaling[1, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                      np.sum(self.__filter_kernel[start_even::2]))

        scaling[1, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                      np.sum(self.__filter_kernel[start_odd::2]))

        expand_image = np.zeros(laplace_image.shape, dtype='float')
        expand_image[::2, ::2] = base_image
        expand_image = cv2.filter2D(src=expand_image,
                                    ddepth=-1,
                                    kernel=np.float32(kernel_2d),
                                    borderType=cv2.BORDER_REFLECT_101)

        # Adapt using the scaling
        expand_image[0::2, 0::2] *= scaling[0, 0]
        expand_image[0::2, 1::2] *= scaling[0, 1]
        expand_image[1::2, 0::2] *= scaling[1, 0]
        expand_image[1::2, 1::2] *= scaling[1, 1]

        image = expand_image + laplace_image - self.__default_value

        return np.round(image).astype(base_image.dtype)

    # --------------------------------------------------------------
    def do_inverse_laplace_3D(self,
                              base_image: np.ndarray,
                              laplace_image: np.ndarray) -> np.ndarray:
        """
        Recreate an image out of the base (=reduced) and the laplace image

        Filtering is done using openCV filter2d with BORDER_REFLECT_101,
        which means gfedcb|abcdefgh|gfedcba

        Parameters
        ----------
        base_image : np.ndarray
            Base image.
        laplace_image : np.ndarray
            Laplace image.

        Returns
        -------
        np.ndarray
            The recreated image with the same size than the laplace image.

        """

        kernel_2d = np.outer(self.__filter_kernel, self.__filter_kernel)
        d, h, w = laplace_image.shape
        R = self.__filter_radius

        # Do the expand
        # In EXPAND a scaling is needed, because not all pixels are used
        # Either all even or all odd elemnts of the kernel are used
        # So there are 2 states and since we are in 3D, it results in 8 cases

        start_even = self.__filter_radius % 2
        start_odd = (self.__filter_radius + 1) % 2

        scaling = np.zeros((2, 2, 2), dtype='float')
        scaling[0, 0, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[0, 0, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        scaling[0, 1, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[0, 1, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        scaling[1, 0, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[1, 0, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        scaling[1, 1, 0] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_even::2]))

        scaling[1, 1, 1] = Decimal(1) / (np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]) *
                                         np.sum(self.__filter_kernel[start_odd::2]))

        expand_image = np.zeros(laplace_image.shape, dtype='float')
        expand_image[::2, ::2, ::2] = base_image

        for t_h in range(h):
            for t_w in range(w):
                curLine = np.copy(expand_image[:, t_h, t_w])
                curLine = np.concatenate((curLine[R:0:-1],
                                          curLine,
                                          curLine[(d-2):(d-2-R):-1]))
                expand_image[:, t_h, t_w] = np.convolve(curLine,
                                                        np.float32(self.__filter_kernel),
                                                        'same')[R:(d+R)]
        for t_d in range(d):
            expand_image[t_d] = cv2.filter2D(src=expand_image[t_d],
                                             ddepth=-1,
                                             kernel=np.float32(kernel_2d),
                                             borderType=cv2.BORDER_REFLECT_101)

        # Adapt using the scaling
        expand_image[0::2, 0::2, 0::2] *= scaling[0, 0, 0]
        expand_image[0::2, 0::2, 1::2] *= scaling[0, 0, 1]
        expand_image[0::2, 1::2, 0::2] *= scaling[0, 1, 0]
        expand_image[0::2, 1::2, 1::2] *= scaling[0, 1, 1]
        expand_image[1::2, 0::2, 0::2] *= scaling[1, 0, 0]
        expand_image[1::2, 0::2, 1::2] *= scaling[1, 0, 1]
        expand_image[1::2, 1::2, 0::2] *= scaling[1, 1, 0]
        expand_image[1::2, 1::2, 1::2] *= scaling[1, 1, 1]

        image = expand_image + laplace_image - self.__default_value

        return np.round(image).astype(base_image.dtype)


# -------------------------------------------------------------------------
def merge_tiles(image_tiles: List,
                image_size: Tuple = (1024, 1024),
                tile_size: Tuple = (512, 512)) -> List:

    """
    Combine the splitted image tiles to a list of images

    Parameters
    ----------
    image_tiles : List
        The tiled 3D image stack.
    image_size : Tuple, optional
        Size of the actual image planes (height, width).
        The default is (1024, 1024).
    tile_size : Tuple, optional
        Size of the tiles (height, width).
        The default is (512, 512).

    Raises
    ------
    ValueError
        When tile size does not fit into image size.

    Returns
    -------
    images : List[np.ndarray]
        A list of 2D images.

    """

    if (image_size[0] % tile_size[0]) != 0 or (image_size[1] % tile_size[1]) != 0:
        print("Invalid sizes, tile size must fit into image size")
        raise ValueError

    tiles_vertical = int(image_size[0] / tile_size[0])
    tiles_horizontal = int(image_size[1] / tile_size[1])

    images = []
    curImage = np.zeros(image_size, dtype=image_tiles[0].dtype)

    for d in range(0, len(image_tiles), (tiles_horizontal * tiles_vertical)):
        for h in range(tiles_vertical):
            for w in range(tiles_horizontal):
                curImage[h*tile_size[0]:(h+1)*tile_size[0],
                         w*tile_size[1]:(w+1)*tile_size[1]] = image_tiles[d + h*tiles_horizontal + w]

        images.append(np.copy(curImage))

    return images
