This project contains python scripts and classes to create a 3D DICOM Gauß-Laplacian image pyramid (GLP)

# Usage
The main script is ***acquire_image_stack.py*** which performs the GLP creation and saves it as DICOM files.
Therefore it is possible to either record the images directly from a PCO camera using pco.python package,
or to read the raw image from (Multi)DICOM file(s) or (Milti)Tiff files(s)

Additionally it is possible to do an extended noise equilibration to compress these images to 8 bit.  
Method is described in  
<cite>KIRCHHÖFER, Daniel M., et al. Extended noise equalisation for image compression in microscopical applications. tm-Technisches Messen, 2019, 86. Jg., Nr. 7-8, S. 422-432.</cite>

All settings have to be configured inside a config.yml file.

## Folder and file structure:
- The tool creates one folder per pyramid level
- Each of this folders contains a list of DICOM files, one file per image plane
- Each files contains a list of image segments, where the segment size is always as big as the image at the highest pyramid level (peak image)

## Reconstruction
For testing purposes the original images can be reconstructed as a python memmap file from the GLP with ***reconstruct_image.py***
