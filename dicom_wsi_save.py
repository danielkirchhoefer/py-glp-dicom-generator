# -*- coding: utf-8 -*-
"""
Created on Sat Mar  7 10:08:12 2020

@author: danie
"""

import pydicom as pd
from pydicom.dataset import Dataset
from pydicom.sequence import Sequence
from datetime import datetime
from typing import Dict, Tuple, List
import numpy as np
import cv2
import math
import os
import imagecodecs

# Many thanks to the Medical Connections for offering free
# valid UIDs (http://www.medicalconnections.co.uk/FreeUID.html)
# Their service was used to obtain the following base UID for pco:
PCO_BASE_UID = '1.2.826.0.1.3680043.10.397.'
PCO_SOP_UID = '1.2.826.0.1.3680043.10.397.0.'
PCO_IMPLEMENTATION_UID = '1.2.826.0.1.3680043.10.397.1.1' # 0 is the file dll
PCO_STUDY_UID = '1.2.826.0.1.3680043.10.397.2.'
PCO_SERIES_UID = '1.2.826.0.1.3680043.10.397.3.'
PCO_DIMENSION_UID = '1.2.826.0.1.3680043.10.397.4.'
PCO_SPECIMEN_UID = '1.2.826.0.1.3680043.10.397.5.'
# Defined in dicom nema standard
WSI_UID = '1.2.840.10008.5.1.4.1.1.77.1.6'


class dicom_wsi_pyramid:

    # -------------------------------------------------------------------------
    def __init__(self,
                 result_folder: str,
                 file_name: str = 'instance',
                 compression: str = 'None'):
        """
        Initialize class members, especially the dataset

        Parameters
        ----------
        result_folder : str
            Path to a folder where the pyramid will be saved.
        file_name : str, optional
            Base file name for the files to be created.
            A number for the specific instance will be added automatically
        compression : str, optional
            Compression method to be used.
            The default is 'None'.

        Raises
        ------
        ValueError
            When the transferred compression code string is unknown.

        Returns
        -------
        None.

        """

        # member variables
        self.__tile_size = (1, 1)
        self.__pixel_size_um = (1, 1, 1)
        self.__result_folder = result_folder
        self.__base_name = file_name
        self.__dtype = 'uint16'
        self.__compression = compression

        # Init file meta
        self.__file_meta = Dataset()
		# Should automatically be set by pydicom
        #self.__file_meta.FileMetaInformationGroupLength = 204
        #self.__file_meta.FileMetaInformationVersion = b'\x00\x01'
        self.__file_meta.MediaStorageSOPClassUID = WSI_UID
        #self.__file_meta.MediaStorageSOPInstanceUID = PCO_SOP_UID  # is filled per file, not here
        if compression == 'None':
            self.__file_meta.TransferSyntaxUID = pd.uid.ExplicitVRLittleEndian
        elif compression == 'JPEG':
            self.__file_meta.TransferSyntaxUID = pd.uid.JPEGBaseline
        elif compression == 'JPEGLS':
            self.__file_meta.TransferSyntaxUID = pd.uid.JPEGLSLossless
        elif compression == 'JPEG2000':
            self.__file_meta.TransferSyntaxUID = pd.uid.JPEG2000Lossless
        elif compression == 'RLE':
            self.__file_meta.TransferSyntaxUID = pd.uid.RLELossless
        else:
            print("Selected compression not supported")
            raise ValueError

        self.__file_meta.ImplementationClassUID = PCO_IMPLEMENTATION_UID
        self.__file_meta.ImplementationVersionName = 'PCO_WSI_DICOM'

        # Init Dataset
        # UIDs
        self.__ds = Dataset()
        self.__ds.ImageType = ['DERIVED', 'PRIMARY', 'VOLUME', 'RESAMPLED']
        self.__ds.SOPClassUID = WSI_UID

        # self.ds.SOPInstanceUID = PCO_SOP_UID
        self.__ds.StudyInstanceUID = pd.uid.generate_uid(prefix=PCO_STUDY_UID)
        self.__ds.SeriesInstanceUID = pd.uid.generate_uid(prefix=PCO_SERIES_UID)

        self.__ds.Modality = 'SM'
        self.__ds.VolumetricProperties = 'VOLUME'

        # Patient
        self.__ds.PatientName = 'PCO^AG'
        self.__ds.PatientID = '000-00-0001'
        self.__ds.PatientBirthDate = '19870101'
        self.__ds.PatientSex = 'M'
        self.__ds.PatientOrientation = ''
        
        # General Study
        self.__ds.ReferringPhysicianName = 'Albert^Einstein'
        self.__ds.AccessionNumber = 'QRCodeScan?'
        # General Equipment
        self.__ds.Manufacturer = 'PCO AG'
        # EnhancedGeneralEquipment
        self.__ds.ManufacturerModelName = 'Light Scanning Microscope'
        self.__ds.DeviceSerialNumber = '123456789'
        self.__ds.SoftwareVersions = '1.00'
        # Acquisition Context
        acquisition_context_sequence = Sequence()  # Currently stays empty
        self.__ds.AcquisitionContextSequence =acquisition_context_sequence
        # Specimen Makro
        self.__ds.ContainerIdentifier = 'Hier kommt der Container hin'
        issuer_container_identifier_sequence = Sequence()  # Currently stays empty
        self.__ds.IssuerOfTheContainerIdentifierSequence = issuer_container_identifier_sequence
        container_type_code_sequence = Sequence()  # Currently stays empty
        self.__ds.ContainerTypeCodeSequence = container_type_code_sequence
        specimen_description_sequence = Sequence()
        self.__ds.SpecimenDescriptionSequence = specimen_description_sequence
        specimen_descriptor = Dataset()
        specimen_descriptor.SpecimenIdentifier = 'Hier kommt die Probe hin'
        issuer_specimen_identifier_sequence = Sequence()  # Currently stays empty
        specimen_descriptor.IssuerOfTheSpecimenIdentifierSequence = issuer_specimen_identifier_sequence
        specimen_descriptor.SpecimenUID = pd.uid.generate_uid(prefix=PCO_SPECIMEN_UID)
        specimen_preparation_sequence = Sequence()  # Currently stays empty
        specimen_descriptor.SpecimenPreparationSequence = specimen_preparation_sequence
        specimen_description_sequence.append(specimen_descriptor)
        
        # WholeSlideMicroscopyImage
        self.__ds.PresentationLUTShape = 'IDENTITY'
        self.__ds.RescaleIntercept = '0'
        self.__ds.RescaleSlope = '1.0'
        self.__ds.SpecimenLabelInImage = 'NO'
        self.__ds.BurnedInAnnotation = 'NO'
        self.__ds.FocusMethod = 'MANUAL'
        self.__ds.ExtendedDepthOfField = 'NO'
        
        # Dimension organization Sequence
        dim_uid = pd.uid.generate_uid(prefix=PCO_DIMENSION_UID)
        dimension_organization_sequence = Sequence()
        self.__ds.DimensionOrganizationSequence = dimension_organization_sequence
        # Dimension Organization Sequence: Dimension Organization 1
        dimension_organization1 = Dataset()
        dimension_organization1.DimensionOrganizationUID = dim_uid
        dimension_organization1.DimensionOrganizationType = 'TILED_FULL'
        dimension_organization_sequence.append(dimension_organization1)

        # Dimension Index Sequence
        dimension_index_sequence = Sequence()
        self.__ds.DimensionIndexSequence = dimension_index_sequence
        # Dimension Index 1 is plane
        dimension_index1 = Dataset()
        dimension_index1.DimensionOrganizationUID = dim_uid
        dimension_index1.DimensionIndexPointer = pd.tag.Tag(0x0040, 0x074A)
        dimension_index1.FunctionalGroupPointer = pd.tag.Tag(0x0048, 0x021a)
        dimension_index1.DimensionDescriptionLabel = 'Z Offset in Slide Coordinate System'
        dimension_index_sequence.append(dimension_index1)
        # Dimension Index 2 is row
        dimension_index2 = Dataset()
        dimension_index2.DimensionOrganizationUID = dim_uid
        dimension_index2.DimensionIndexPointer = pd.tag.Tag(0x0040, 0x073A)
        dimension_index2.FunctionalGroupPointer = pd.tag.Tag(0x0048, 0x021a)
        dimension_index2.DimensionDescriptionLabel = 'Y Offset in Slide Coordinate System'
        dimension_index_sequence.append(dimension_index2)
        # Dimension Index 3 is column
        dimension_index3 = Dataset()
        dimension_index3.DimensionOrganizationUID = dim_uid
        dimension_index3.DimensionIndexPointer = pd.tag.Tag(0x0040, 0x072A)
        dimension_index3.FunctionalGroupPointer = pd.tag.Tag(0x0048, 0x021a)
        dimension_index3.DimensionDescriptionLabel = 'X Offset in Slide Coordinate System'
        dimension_index_sequence.append(dimension_index3)

        # Image information has to be filled later
        # self.__instanceNumber = "1"
        # self.__ds.SamplesPerPixel = 1
        # self.__ds.PhotometricInterpretation = 'MONOCHROME2'
        # self.__ds.PlanarConfiguration = 0
        # self.__ds.NumberOfFrames = "6"
        # self.__ds.Rows = 512
        # self.__ds.Columns = 512
        # self.__ds.BitsAllocated = 8
        # self.__ds.BitsStored = 8
        # self.__ds.HighBit = 7
        # self.__ds.PixelRepresentation = 0
        # self.__ds.ImagedVolumeWidth = 15.0
        # self.__ds.ImagedVolumeHeight = 15.0
        # self.__ds.ImagedVolumeDepth = 1.0
        # self.__ds.TotalPixelMatrixColumns = 1120
        # self.__ds.TotalPixelMatrixRows = 792

        # Total Pixel Matrix Origin Sequence
        total_pixel_matrix_origin_sequence = Sequence()
        self.__ds.TotalPixelMatrixOriginSequence = total_pixel_matrix_origin_sequence

        # Total Pixel Matrix Origin Sequence: Total Pixel Matrix Origin 1
        total_pixel_matrix_origin1 = Dataset()
        total_pixel_matrix_origin1.XOffsetInSlideCoordinateSystem = "0"
        total_pixel_matrix_origin1.YOffsetInSlideCoordinateSystem = "0"
        total_pixel_matrix_origin1.ZOffsetInSlideCoordinateSystem = "0"
        total_pixel_matrix_origin_sequence.append(total_pixel_matrix_origin1)

        self.__ds.ImageOrientationSlide = ['0', '1', '0', '1', '0', '0']

        # Optical Path Sequence
        optical_path_sequence = Sequence()
        self.__ds.OpticalPathSequence = optical_path_sequence

        # Optical Path Sequence: Optical Path 1
        optical_path1 = Dataset()

        # Illumination Type Code Sequence
        illumination_type_code_sequence = Sequence()
        optical_path1.IlluminationTypeCodeSequence = illumination_type_code_sequence

        # Illumination Type Code Sequence: Illumination Type Code 1
        illumination_type_code1 = Dataset()
        illumination_type_code1.CodeValue = '111744'
        illumination_type_code1.CodingSchemeDesignator = 'DCM'
        illumination_type_code1.CodeMeaning = 'Brightfield illumination'
        illumination_type_code_sequence.append(illumination_type_code1)

        # optical_path1.ICCProfile = b'00\x00\x00'  would be required for color
        optical_path1.OpticalPathIdentifier = '1'
        optical_path1.OpticalPathDescription = 'Brightfield'

        # Illumination Color Code Sequence
        illumination_color_code_sequence = Sequence()
        optical_path1.IlluminationColorCodeSequence = illumination_color_code_sequence

        # Illumination Color Code Sequence: Illumination Color Code 1
        illumination_color_code1 = Dataset()
        illumination_color_code1.CodeValue = 'R-102C0'
        illumination_color_code1.CodingSchemeDesignator = 'SCT'
        illumination_color_code1.CodeMeaning = 'Full Spectrum'
        illumination_color_code_sequence.append(illumination_color_code1)
        optical_path_sequence.append(optical_path1)

        self.__ds.NumberOfOpticalPaths = 1

        # Shared Functional Groups Sequence
        shared_functional_groups_sequence = Sequence()
        self.__ds.SharedFunctionalGroupsSequence = shared_functional_groups_sequence
        # Shared Functional Groups Sequence: Shared Functional Groups 1
        shared_functional_groups1 = Dataset()

        # Pixel Measures Sequence
        pixel_measures_sequence = Sequence()
        shared_functional_groups1.PixelMeasuresSequence = pixel_measures_sequence

        # Pixel Measures Sequence: Pixel Measures 1
        pixel_measures1 = Dataset()
        pixel_measures1.SliceThickness = '{}'.format(self.__pixel_size_um[0] / 1000)
        # This somehow doesn't work
        # pixel_measures1.SpacingBetweenSlices = '{}'.format(self.__pixel_size_um[0] / 1000)
        pixel_measures1.PixelSpacing = ['{}'.format(self.__pixel_size_um[2] / 1000),
                                        '{}'.format(self.__pixel_size_um[1] / 1000)]
        pixel_measures_sequence.append(pixel_measures1)

        # Whole Slide Microscopy Image Frame Type Sequence
        whole_slide_microscopy_image_frame_type_sequence = Sequence()
        shared_functional_groups1.WholeSlideMicroscopyImageFrameTypeSequence = whole_slide_microscopy_image_frame_type_sequence
        # Whole Slide Microscopy Image Frame Type Sequence: Whole Slide Microscopy Image Frame Type 1
        whole_slide_microscopy_image_frame_type1 = Dataset()
        whole_slide_microscopy_image_frame_type1.FrameType = ['DERIVED',
                                                              'PRIMARY',
                                                              'VOLUME',
                                                              'RESAMPLED']
        whole_slide_microscopy_image_frame_type_sequence.append(whole_slide_microscopy_image_frame_type1)

        # Optical Path Identification Sequence
        optical_path_identification_sequence = Sequence()
        shared_functional_groups1.OpticalPathIdentificationSequence = optical_path_identification_sequence
        # Optical Path Identification Sequence: Optical Path Identification 1
        optical_path_identification1 = Dataset()
        optical_path_identification1.OpticalPathIdentifier = '1'
        optical_path_identification_sequence.append(optical_path_identification1)
        shared_functional_groups_sequence.append(shared_functional_groups1)

    # -------------------------------------------------------------------------
    def set_image_info(self,
                       image_size,
                       pixel_size: Tuple,
                       tile_size: Tuple,
                       levels: int,
                       isotropy_level: int,
                       bits_per_pixel: int,
                       filter_kernel: np.ndarray,
                       meta: Dict = None):
        """
        Fill the dataset and members with image information

        Parameters
        ----------
        image_size : Tuple(int, int, int)
            Tupel defining the size of the initial image (d,h,w).
        pixel_size : Tuple(float, float, float)
            Tupel defining the size of a voxel in um.
        tile_size : Tuple(int, int)
            Tupel defining the x-y size of one image tile
            This will be the size of the highest pyramid level.
        levels : int
            Number of levels in total.
        isotropy_level : int
            Index of level where the 3D reduction starts/ends
        bits_per_pixel : int
            Number of bits per pixel for the image.
        filter_kernel : np.ndarray
            Kernel (1D) used for the pyramid generation.
        meta : Dict, optional
            Metadata structure of the image, containing the timestamp.
            The default is None.

        Raises
        ------
        ValueError
            When bits_per_pixel is larger than 12 and compression is JPEG.

        Returns
        -------
        None.

        """

        if self.__compression == 'JPEG' and bits_per_pixel > 12:
            print("JPEG compression only supports up to 12 bit")
            raise ValueError

        # Fill Member Infos
        self.__pixel_size_um = pixel_size
        self.__tile_size = tile_size
        self.__levels = levels
        self.__isotropy_level = isotropy_level

        # Update pixel size information
        # Pixel Measures Sequence: Pixel Measures 1
        pixel_measures1 = Dataset()
        pixel_measures1.SliceThickness = '{}'.format(self.__pixel_size_um[0] / 1000)
        # pixel_measures1.SpacingBetweenSlices = '{}'.format(self.__pixel_size_um[0] / 1000)
        pixel_measures1.PixelSpacing = ['{}'.format(self.__pixel_size_um[2] / 1000),
                                        '{}'.format(self.__pixel_size_um[1] / 1000)]
        self.__ds.SharedFunctionalGroupsSequence[0].PixelMeasuresSequence[0] = pixel_measures1

        # Fill image information as far as posible
        self.__ds.ConvolutionKernel = np.array2string(filter_kernel,
                                                      precision=5,
                                                      separator='\\',
                                                      suppress_small=True).strip('[]')
        self.__ds.SamplesPerPixel = 1
        # self.__ds.PlanarConfiguration = 0 Only for samples per pixel > 1
        self.__ds.PhotometricInterpretation = 'MONOCHROME2'
        self.__ds.LossyImageCompression = '00'  # No lossy compression

        self.__ds.PixelRepresentation = 0
        self.__ds.ImagedVolumeDepth = image_size[0] * pixel_size[0] / 1000
        self.__ds.ImagedVolumeHeight = image_size[1] * pixel_size[1] / 1000
        self.__ds.ImagedVolumeWidth = image_size[2] * pixel_size[2] / 1000

        self.__ds.Rows = tile_size[0]
        self.__ds.Columns = tile_size[1]

        self.__ds.BitsStored = bits_per_pixel
        self.__ds.HighBit = bits_per_pixel - 1
        # self.__instanceNumber = "1"
        # self.__ds.NumberOfFrames = "6"
        # self.__ds.TotalPixelMatrixColumns = 1120
        # self.__ds.TotalPixelMatrixRows = 792

        if bits_per_pixel > 8:
            self.__ds.BitsAllocated = 16
            self.__dtype = 'uint16'
        else:  # if bits_per_pixel <= 8:
            self.__ds.BitsAllocated = 8
            self.__dtype = 'uint8'

        # Fill Date and Time info
        now = datetime.now()
        if meta is not None:
            now = meta['time']

        # Just to have it, there would not be a need of it
        self.__ds.StudyID = now.strftime("%Y%m%d_%H%M%S")
        self.__ds.SeriesNumber = now.strftime("%H%M%S")

        self.__ds.StudyDate = now.strftime("%Y%m%d")
        self.__ds.SeriesDate = now.strftime("%Y%m%d")
        self.__ds.ContentDate = now.strftime("%Y%m%d")
        self.__ds.AcquisitionDateTime = now.strftime("%Y%m%d%H%M%S.000000")
        self.__ds.StudyTime = now.strftime("%H%M%S.000000")
        self.__ds.SeriesTime = now.strftime("%H%M%S.000000")
        self.__ds.ContentTime = now.strftime("%H%M%S.000000")
        # ds.InstanceCreationDate = '20191114'
        # ds.InstanceCreationTime = '134121.000000'

    # -------------------------------------------------------------------------
    def save_pyramid_level(self,
                           image: np.ndarray,
                           level: int,
                           pixel_size_um: Tuple):
        """
        Save the image representing the current level to file
        Each plane is saved individually
        Therefore a subfolder is created

        Parameters
        ----------
        image : np.ndarray
            Image containing the current level.
        level : int
            Level of the image.
        pixel_size_um : Tuple(float, float, float)
            Tupel defining the current size of a voxel in um.

        Returns
        -------
        None.

        """

        d, h, w = image.shape
        tile_count = (int(np.ceil(h/self.__tile_size[0])) *
                      int(np.ceil(w/self.__tile_size[1])))

        # Split image into tiles
        for td in range(d):
            dataset = self.__ds
            file_meta = self.__file_meta
    
            sop_instance_uid = pd.uid.generate_uid(prefix=PCO_SOP_UID)
            file_meta.MediaStorageSOPInstanceUID = sop_instance_uid
    
            # Create Private tags 
            # we use 0x0049 since volume and pixel dims are stored in 0x0048
            pyramid_block = dataset.private_block(0x0049,
                                                  "Laplace Pyramid Info",
                                                  create=True)
            pyramid_block.add_new(0x01, "FL", pixel_size_um[2]) # x size um
            pyramid_block.add_new(0x02, "FL", pixel_size_um[1]) # y size um
            pyramid_block.add_new(0x03, "FL", pixel_size_um[0]) # z size um
            pyramid_block.add_new(0x11, "US", level)
            pyramid_block.add_new(0x12, "US", self.__isotropy_level)
            pyramid_block.add_new(0x13, "US", self.__levels)
    
            # Fill instance info
            now = datetime.now()
            dataset.SOPInstanceUID = sop_instance_uid
            dataset.InstanceCreationDate = now.strftime("%Y%m%d")
            dataset.InstanceCreationTime = now.strftime("%H%M%S.000000")
    
            # Fill the rest of the image information
            # Combined info of level and plane
            dataset.InstanceNumber = "{:02d}{:04d}".format(level + 1, td + 1)
            dataset.NumberOfFrames = "{}".format(tile_count)
            dataset.TotalPixelMatrixFocalPlanes = 1
            dataset.TotalPixelMatrixRows = h
            dataset.TotalPixelMatrixColumns = w
    
            # Per-frame Functional Groups Sequence
            per_frame_functional_groups_sequence = Sequence()
            dataset.PerFrameFunctionalGroupsSequence = per_frame_functional_groups_sequence

            # Get Min Max values
            dataset.SmallestImagePixelValue = np.min(image[td]).item() # We need native
            dataset.LargestImagePixelValue = np.max(image[td]).item() # We need native
            
            # Create the subdirectory
            sub_dir_path = self.__result_folder + self.__base_name + '{:02d}'.format(level)
            if not os.path.exists(sub_dir_path):
                os.mkdir(sub_dir_path)
    
            # Split image into tiles
            image_tiles, pixel_positions = create_tiles(image[td],
                                                        self.__tile_size[0],
                                                        self.__tile_size[1])

            idx_row = 1
            idx_col = 1
            idx_plane = td + 1
            for i in range(len(pixel_positions)):
                rowPos, colPos = pixel_positions[i]
                # Per-frame Functional Groups Sequence: Per-frame Functional Groups 1
                per_frame_functional_groups = Dataset()

                # Frame Content Sequence
                frame_content_sequence = Sequence()
                per_frame_functional_groups.FrameContentSequence = frame_content_sequence

                # Frame Content Sequence: Frame Content 1
                frame_content = Dataset()
                frame_content.DimensionIndexValues = [idx_plane,
                                                      idx_row,
                                                      idx_col]
                frame_content_sequence.append(frame_content)

                # Plane Position (Slide) Sequence
                plane_position_slide_sequence = Sequence()
                per_frame_functional_groups.PlanePositionSlideSequence = plane_position_slide_sequence

                # Plane Position (Slide) Sequence: Plane Position (Slide)
                plane_position_slide = Dataset()
                plane_position_slide.ZOffsetInSlideCoordinateSystem = td * pixel_size_um[0]  # This is in um
                plane_position_slide.YOffsetInSlideCoordinateSystem = rowPos * pixel_size_um[1] / 1000
                plane_position_slide.XOffsetInSlideCoordinateSystem = colPos * pixel_size_um[2] / 1000

                plane_position_slide.ColumnPositionInTotalImagePixelMatrix = colPos + 1
                plane_position_slide.RowPositionInTotalImagePixelMatrix = rowPos + 1

                plane_position_slide_sequence.append(plane_position_slide)
                per_frame_functional_groups_sequence.append(per_frame_functional_groups)

                # Check if it is a row switch
                if i < (len(pixel_positions) - 1) and pixel_positions[i+1][0] != rowPos:
                    idx_col = 1
                    idx_row += 1
                else:
                    idx_col += 1
        
            dataset.file_meta = file_meta
            dataset.is_implicit_VR = False
            dataset.is_little_endian = True
    
            full_path = sub_dir_path + '/L{:02d}_S{:04d}.dcm'.format(level, td)
    
            if self.__compression == 'None':
                dataset.PixelData = image_tiles.flatten().tobytes()
                dataset.save_as(full_path, write_like_original=False)
            else:
                img_byte_list = []
                if self.__compression == 'RLE':
                    for tile in image_tiles:
                        img_byte_list.append(pd.pixel_data_handlers.rle_handler.rle_encode_frame(tile))
    
                elif self.__compression == 'JPEG':
                    dataset.LossyImageCompression = '01'  # Lossy compression
                    dataset.LossyImageCompressionMethod = 'ISO_10918_1'
    
                    for tile in image_tiles:
                        buf = imagecodecs.jpeg_encode(tile, 95)  # Default = 90
                        img_byte_list.append(bytes(buf))
    
                elif self.__compression == 'JPEGLS':
                    for tile in image_tiles:
                        buf = imagecodecs.jpegls_encode(tile)
                        img_byte_list.append(bytes(buf))
    
                elif self.__compression == 'JPEG2000':
                    for tile in image_tiles:
                        buf = imagecodecs.jpeg2k_encode(tile)
                        img_byte_list.append(bytes(buf))
    
                # encapsulate
                dataset.PixelData =  pd.encaps.encapsulate([x for x in img_byte_list])
                dataset['PixelData'].is_undefined_length = True
                dataset.save_as(full_path, write_like_original=False)

    # Peak level will be saved in one single file
    def save_peak_level(self,
                        image: np.ndarray,
                        level: int,
                        pixel_size_um: Tuple):
        """
        Save the image representing the peak level to file
        In this file also private blocks are created
        for storing relevant pyramid info

        Parameters
        ----------
        image : np.ndarray
            Image containing the current level.
        level : int
            Level of the image.
        pixel_size_um : Tuple(float, float, float)
            Tupel defining the current size of a voxel in um.

        Returns
        -------
        None.

        """

        d, h, w = image.shape
        tile_count = (int(np.ceil(h/self.__tile_size[0])) *
                      int(np.ceil(w/self.__tile_size[1])))

        dataset = self.__ds
        file_meta = self.__file_meta

        sop_instance_uid = pd.uid.generate_uid(prefix=PCO_SOP_UID)
        file_meta.MediaStorageSOPInstanceUID = sop_instance_uid

        # Create Private tags 
        # we use 0x0049 since volume and pixel dims are stored in 0x0048
        pyramid_block = dataset.private_block(0x0049,
                                              "Laplace Pyramid Info",
                                              create=True)
        pyramid_block.add_new(0x01, "FL", pixel_size_um[2]) # x size um
        pyramid_block.add_new(0x02, "FL", pixel_size_um[1]) # y size um
        pyramid_block.add_new(0x03, "FL", pixel_size_um[0]) # z size um
        pyramid_block.add_new(0x11, "US", level)
        pyramid_block.add_new(0x12, "US", self.__isotropy_level)
        pyramid_block.add_new(0x13, "US", self.__levels)

        # Fill instance info
        now = datetime.now()
        dataset.SOPInstanceUID = sop_instance_uid
        dataset.InstanceCreationDate = now.strftime("%Y%m%d")
        dataset.InstanceCreationTime = now.strftime("%H%M%S.000000")

        # Fill the rest of the image information
        dataset.InstanceNumber = "{:02d}{:04d}".format(level + 1, 0)
        dataset.NumberOfFrames = "{}".format(d * tile_count)
        dataset.TotalPixelMatrixFocalPlanes = d
        dataset.TotalPixelMatrixRows = h
        dataset.TotalPixelMatrixColumns = w

        # Get Min Max values
        dataset.SmallestImagePixelValue = np.min(image).item() # We need native
        dataset.LargestImagePixelValue = np.max(image).item() # We need native

        # Create the specific Datasets
        image_tile_array = np.zeros((d,
                                    tile_count,
                                    self.__tile_size[0],
                                    self.__tile_size[1]),
                                   dtype=self.__dtype)
        # Per-frame Functional Groups Sequence
        per_frame_functional_groups_sequence = Sequence()
        dataset.PerFrameFunctionalGroupsSequence = per_frame_functional_groups_sequence

        # Split image into tiles
        for td in range(d):
            # Split image into tiles
            image_tiles, pixel_positions = create_tiles(image[td],
                                                        self.__tile_size[0],
                                                        self.__tile_size[1])

            image_tile_array[td] = image_tiles

            idx_row = 1
            idx_col = 1
            idx_plane = td + 1
            for i in range(len(pixel_positions)):
                rowPos, colPos = pixel_positions[i]
                # Per-frame Functional Groups Sequence: Per-frame Functional Groups 1
                per_frame_functional_groups = Dataset()

                # Frame Content Sequence
                frame_content_sequence = Sequence()
                per_frame_functional_groups.FrameContentSequence = frame_content_sequence

                # Frame Content Sequence: Frame Content 1
                frame_content = Dataset()
                frame_content.DimensionIndexValues = [idx_plane,
                                                      idx_row,
                                                      idx_col]
                frame_content_sequence.append(frame_content)

                # Plane Position (Slide) Sequence
                plane_position_slide_sequence = Sequence()
                per_frame_functional_groups.PlanePositionSlideSequence = plane_position_slide_sequence

                # Plane Position (Slide) Sequence: Plane Position (Slide)
                plane_position_slide = Dataset()
                plane_position_slide.ZOffsetInSlideCoordinateSystem = td * pixel_size_um[0]  # This is in um
                plane_position_slide.YOffsetInSlideCoordinateSystem = rowPos * pixel_size_um[1] / 1000
                plane_position_slide.XOffsetInSlideCoordinateSystem = colPos * pixel_size_um[2] / 1000

                plane_position_slide.ColumnPositionInTotalImagePixelMatrix = colPos + 1
                plane_position_slide.RowPositionInTotalImagePixelMatrix = rowPos + 1

                plane_position_slide_sequence.append(plane_position_slide)
                per_frame_functional_groups_sequence.append(per_frame_functional_groups)

                # Check if it is a row switch
                if i < (len(pixel_positions) - 1) and pixel_positions[i+1][0] != rowPos:
                    idx_col = 1
                    idx_row += 1
                else:
                    idx_col += 1
        
        dataset.file_meta = file_meta
        dataset.is_implicit_VR = False
        dataset.is_little_endian = True

        full_path = self.__result_folder + self.__base_name + '_peak_{}.dcm'.format(level)

        if self.__compression == 'None':
            dataset.PixelData = image_tile_array.flatten().tobytes()
            dataset.save_as(full_path, write_like_original=False)
        else:
            img_byte_list = []
            if self.__compression == 'RLE':
                for plane in image_tile_array:
                    for tile in plane:
                        img_byte_list.append(pd.pixel_data_handlers.rle_handler.rle_encode_frame(tile))

            elif self.__compression == 'JPEG':
                dataset.LossyImageCompression = '01'  # Lossy compression
                dataset.LossyImageCompressionMethod = 'ISO_10918_1'

                for plane in image_tile_array:
                    for tile in plane:
                        buf = imagecodecs.jpeg_encode(tile, 95)  # Default = 90
                        img_byte_list.append(bytes(buf))

            elif self.__compression == 'JPEGLS':
                for plane in image_tile_array:
                    for tile in plane:
                        buf = imagecodecs.jpegls_encode(tile)
                        img_byte_list.append(bytes(buf))

            elif self.__compression == 'JPEG2000':
                for plane in image_tile_array:
                    for tile in plane:
                        buf = imagecodecs.jpeg2k_encode(tile)
                        img_byte_list.append(bytes(buf))

            # encapsulate
            dataset.PixelData =  pd.encaps.encapsulate([x for x in img_byte_list])
            dataset['PixelData'].is_undefined_length = True
            dataset.save_as(full_path, write_like_original=False)

# -------------------------------------------------------------------------
def create_tiles(image: np.ndarray,
                 tileSizeY: int = 512,
                 tileSizeX: int = 512) -> [np.ndarray, List]:
    """
    Split the transferred image into tiles
    The function also computes the slide offset list required for dicom

    Parameters
    ----------
    image : np.ndarray
        The original image to be tiled.
    tileSizeY : int, optional
        Height of the tiles. The default is 512.
    tileSizeX : int, optional
        Width of the tiles. The default is 512.

    Raises
    ------
    ValueError
        When the image type is neither uint8 nor uint16.

    Returns
    -------
    image_tiles : np.ndarray
        The image tiles according to the tileSize.
    pixel_positions : List[Tuple(int, int)]
        The start positions in pixel for all tiles.

    """

    if image.dtype != 'uint16' and image.dtype != 'uint8':
        print("Invalid Image format, only uint8 and uint16 are supported")
        raise ValueError

    h, w = image.shape
    pixel_positions = []
    number_of_tiles = math.ceil(h / tileSizeY) * math.ceil(w / tileSizeX)

    if image.dtype == 'uint16':
        image_tiles = np.zeros([number_of_tiles, tileSizeY, tileSizeX],
                               dtype='<u2')
    else:  # if image.dtype == 'uint8'
        image_tiles = np.zeros([number_of_tiles, tileSizeY, tileSizeX],
                               dtype='<u1')

    curPos = 0
    for y in range(0, h, tileSizeY):  # 0 since range stops at h-1
        endPosY = y + tileSizeY
        if endPosY >= h:
            endPosY = h

        for x in range(0, w, tileSizeX):  # 0 since range stops at h-1
            endPosX = x + tileSizeX
            if endPosX >= w:
                endPosX = w

            image_tiles[curPos, :(endPosY - y), :(endPosX - x)] = image[y:endPosY, x:endPosX]
            pixel_positions.append((y, x))
            curPos += 1

    return image_tiles, pixel_positions
